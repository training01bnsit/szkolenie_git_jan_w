#pragma once
#include <string>

class StringUtils
{
public:
	std::string removeSpaces(const std::string& a);
	std::string addSufix(const std::string& a);
	std::string addSpaces(const std::string& a);
	std::string addPrefix(const std::string& a);
	std::string addPrefixoSufix(const std::string& a);
	std::string toLowerCase(const std::string& a);
	void main();
};
